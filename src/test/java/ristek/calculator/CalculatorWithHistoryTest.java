package ristek.calculator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CalculatorWithHistoryTest {

    private ICalculatorRepository calculatorRepository;
    private CalculatorWithHistory calculatorWithHistory;

    @BeforeEach
    void instantiate() {
        calculatorRepository = Mockito.mock(ICalculatorRepository.class);
        calculatorWithHistory = new CalculatorWithHistory(calculatorRepository);
    }

    @Test
    void addShouldAddProperlyAndSaveToRepository() {
        int a = 5;
        int b = 4;
        int expected = 9;

        int actual = calculatorWithHistory.add(a, b);

        assertEquals(expected, actual);
        Mockito.verify(calculatorRepository).save(expected);
    }

    @Test
    void ansShouldReturnLastEntryOfOperation() {
        int expected = 5;
        Mockito.when(calculatorRepository.getLastEntry()).thenReturn(expected);

        int actual = calculatorWithHistory.ans();

        assertEquals(expected, actual);
        Mockito.verify(calculatorRepository).getLastEntry();
    }
}