package ristek.calculator;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.Mockito;

class AppTest {
    // Stub stdout and stderr
    private PrintStream mockStdout;
    private PrintStream mockStderr;
    private final PrintStream originalStdout = System.out;
    private final PrintStream originalStderr = System.err;

    // Stubs are made fresh each test
    @BeforeEach
    void mockOutput() {
        mockStdout = Mockito.mock(PrintStream.class);
        mockStderr = Mockito.mock(PrintStream.class);
        System.setOut(mockStdout);
        System.setErr(mockStderr);
    }

    // Stubs are cleaned up after
    @AfterEach
    void restoreOutput() {
        System.setOut(originalStdout);
        System.setErr(originalStderr);
    }

    @Test
    @DisplayName("Valid Test")
    void validOperationTest() {
        String[] validArgs = {"add", "1", "1"};
        App.main(validArgs);
        Mockito.verify(mockStdout).print(Mockito.eq(2));
    }

    @Test
    @DisplayName("Invalid Operation Test")
    void invalidOperationTest() {
        String[] validArgs = {"foo", "1", "1"};
        App.main(validArgs);
        Mockito.verify(mockStdout).print(Mockito.eq(
                    "Operation is invalid, please input the following: add, substract, multiply, divide"));
    }

    @Test
    @DisplayName("Invalid Number Format Test")
    void invalidNumberFormatTest() {
        String[] validArgs = {"add", "bar", "baz"};
        App.main(validArgs);
        Mockito.verify(mockStdout).print(Mockito.eq(
                    "Invalid number format, For input string: \"bar\""));
    }
}
