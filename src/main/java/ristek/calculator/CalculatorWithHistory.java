package ristek.calculator;

public class CalculatorWithHistory extends Calculator {

    private ICalculatorRepository calculatorRepository;

    public CalculatorWithHistory(ICalculatorRepository calculatorRepository) {
        this.calculatorRepository = calculatorRepository;
    }

    public int add(int a, int b) {
        int result = super.add(a, b);
        calculatorRepository.save(result);
        return result;
    }

    public int ans() {
        return calculatorRepository.getLastEntry();
    }
}
