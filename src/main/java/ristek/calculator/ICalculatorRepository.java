package ristek.calculator;

public interface ICalculatorRepository {
    void save(int result);
    int getLastEntry();
}
